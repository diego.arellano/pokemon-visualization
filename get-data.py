# https://github.com/arnavb/pypokedex
# We'll run this code only once so we don't overuse the API

import pypokedex as pk 
import pandas as pd

pk_dex = []
names = []
heights = []
weights = []
base_experiences = []
pk_types = []
pk_abilities = []
pk_base_stats = []
pk_moves = []

for i in range(150): #0-149
    pokemon = pk.get(dex = i+1)
    pk_dex.append(pokemon.dex)
    names.append(pokemon.name)
    heights.append(pokemon.height)
    weights.append(pokemon.weight)
    base_experiences.append(pokemon.base_experience)
    pk_types.append(pokemon.types)
    pk_abilities.append(pokemon.abilities)
    pk_base_stats.append(pokemon.base_stats)
    pk_moves.append(pokemon.moves)

pk_dex = pd.Series(pk_dex)
names = pd.Series(names)
heights = pd.Series(heights)
weights = pd.Series(weights)
base_experiences = pd.Series(base_experiences)
pk_types = pd.Series(pk_types)
pk_abilities = pd.Series(pk_abilities)
pk_base_stats = pd.Series(pk_base_stats)
pk_moves = pd.Series(pk_moves)

frame = {'dex': pk_dex,
        'name': names,
        'height': heights,
        'weight': weights,
        'base_experience': base_experiences,
        'type': pk_types,
        'abilities': pk_abilities,
        'base_stats': pk_base_stats,
        'moves': pk_moves}

df = pd.DataFrame(frame)

df.to_csv('pokemon.csv', index=False)