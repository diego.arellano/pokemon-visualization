import pandas as pd
from dash import Dash, html, dash_table, Input, Output, callback, dcc
import plotly.graph_objects as go
import re

# Dataframe to work with

df = pd.read_csv('pokemon.csv', index_col=False)
base_stats = df['base_stats'].squeeze()
p = re.compile(r'[a-zA-Z()=_ ]')
base_stats = [p.sub('', x) for x in base_stats]
base_stats = pd.DataFrame(base_stats, columns=['base_stats'])
base_stats = pd.DataFrame(base_stats['base_stats'].str.split(',').values.tolist())

column_names = ['hp', 'attack', 'defense', 'sp_atk', 'sp_def', 'speed']
base_stats.columns = column_names

df.drop(columns=['height', 'weight', 'base_experience', 'type', 'abilities', 'base_stats', 'moves'], inplace=True)

for elem in column_names:
    df[elem] = base_stats[elem].astype('int64')

## Dash

app = Dash(__name__)

app.layout = html.Div([
    html.H1(children='All 150 Pokemon stats', style={'textAlign': 'center'}),
    dash_table.DataTable(
        id='general-table',
        data=df[['dex', 'name']].to_dict('records'),
        sort_action='native',
        # filter_action='native',
        row_selectable='multi',
        selected_rows=[],
        page_size = 10
    ),
    html.Div(
        # html.H2('See the differences in their stats!'),
        dcc.Graph(id='radar-chart')
    ),
])

@callback(
    Output('radar-chart', 'figure'),
    Input('general-table', 'selected_rows')
)

def update_figure(selected_names):
    default_names = [0, 3]
    if selected_names:
        filtered_df = df.loc[selected_names]
    else:
        filtered_df = df.loc[default_names]
        
    fig = go.Figure()
    for row in filtered_df.itertuples():
        fig.add_trace(go.Scatterpolar(
            r = [row.hp, row.attack, row.defense, row.sp_atk, row.sp_def, row.speed],
            theta=column_names,
            fill='toself',
            name=row.name
        ))
    fig.update_layout(
        polar=dict(
            radialaxis=dict(
                visible=False
            )
        ),
        showlegend=True
    )

    return fig


if __name__ == '__main__':
    app.run(debug=True)